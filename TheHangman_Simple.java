import java.util.Scanner;
public class TheHangman_Simple{
	// Notes:
	//This is the simple verison I did in class, in case you're lazy and don't want to look at the other one
	
	public static void main (String[] args){
		
		System.out.println("Let's play Hangman! One player will choose a 4-letter word. The second player will have to guess that word.");
		System.out.print("\nPlayer 1, please choose a 4-letter word:");
		Scanner sc = new Scanner(System.in);
		String target = sc.nextLine();
		System.out.print(String.format("\033[2J"));
		System.out.println("Player 2, you can start guessing by inputing one letter per guess. You have 6 guesses left");
		
		runGame(target);

	}
	//functions
	public static int isLetterinWord(String word, char c){
		//returns postition of  c in word. if c is not  in word, return -1.
		int output=-1;
		for (int i=0;i<word.length();i++){
			
			if (toUpperCase(word.charAt(i))==toUpperCase(c)){
				output=i;
			}
		}
		return output;
	}
	// returns char as uppercase
	public static char toUpperCase(char c){
		return (Character.toString(c).toUpperCase()).charAt(0);
	}
	//prints the letters that have been guessed correctly. If letters hasnt been guessed, substitute with an underline.
	public static void printWork(String word, boolean letter0, boolean letter1, boolean letter2, boolean letter3){
		String output="";
		char Position0= '_';
		char Position1= '_';
		char Position2= '_';
		char Position3= '_';
		if (letter0==true) Position0=word.charAt(0);
		if (letter1==true) Position1=word.charAt(1);
		if (letter2==true) Position2=word.charAt(2);
		if (letter3==true) Position3=word.charAt(3);
		System.out.println("Your result is ["+Position0+" "+Position1+" "+Position2+" "+Position3+" ]");
		
		
	}
	public static void runGame(String word){
		// how many chances there are to guess word
		int guessChances=6;
		
		Scanner scanner = new Scanner(System.in);
		//list of letters that have been guessed or not
		boolean[] Letters_guessed= new boolean[4];

		while(guessChances>=0){ 
		
			//if all letters gussed, break loop and declare winner
			if (Letters_guessed[0]==true&&Letters_guessed[1]==true&&Letters_guessed[2]==true&&Letters_guessed[3]==true){
				System.out.println("Player 2 has won! the 4-letter word was "+word+". They had "+guessChances+" chances left.");
				break;
			}
			// if guesses have run out, break loop and declare winner
			if (guessChances==0){ 
				System.out.println("Player 1 has won! the 4-letter word was "+word+".");
				break;
			}
			System.out.print("\nYour next guess:");
			
			//player guess and the index of where the char is in word
			char Player_guess= toUpperCase(scanner.next().charAt(0));
			int guess_index= isLetterinWord(word,Player_guess);
			
			if (guess_index<0){
				//player  guessed wrongly!
				guessChances--;
				System.out.print("\nYou guessed wrongly.");
				printWork(word,Letters_guessed[0],Letters_guessed[1],Letters_guessed[2],Letters_guessed[3]);
				System.out.println("You have "+guessChances+" chances left.");
				
			}
			else{
				//player guessed right!
				System.out.print("\nYou guessed correctly.");
				Letters_guessed[guess_index]=true;
				printWork(word,Letters_guessed[0],Letters_guessed[1],Letters_guessed[2],Letters_guessed[3]);
			} 
		}
	
	}	
}	